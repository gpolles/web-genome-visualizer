import h5py
import sys
import json
from alabtools.analysis import HssFile
fname = sys.argv[1]
n = int(sys.argv[2])

try:
    with h5py.File(fname, 'r') as f:
        crd = f['coordinates'][n][()].tolist()
        chrom = f['idx'][()].tolist()
        chrom = chrom[:] + chrom[:]
        radius = f['radius'][()].tolist()
        nstruct = len(f['coordinates'])
except:
    with HssFile(fname, 'r') as f:
        crd = f['coordinates'][:, n, :][()].tolist()
        chrom = ['chr%d' % (i+1) for i in f.index.chrom]
        radius = f.radii.tolist()
        nstruct = f.nstruct



cstarts = [0]
for i in range(1, len(chrom)):
	if chrom[i-1] != chrom[i]:
		cstarts.append(i)
cstarts.append(len(chrom))

print json.dumps({
    'crd' : [crd[cstarts[i]:cstarts[i+1]] for i in range(len(cstarts)-1)],
    'idx' : chrom,
    'rad' : [radius[cstarts[i]:cstarts[i+1]] for i in range(len(cstarts)-1)],
    'n' : nstruct,
    'cstarts': cstarts,
    'chroms': [v for i, v in enumerate(chrom) if i == 0 or v != chrom[i-1]],
})
