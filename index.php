<?php 
require('header.php');
?>

<script src="js/bootstrap-slider.min.js"></script>
<script src="libs/jszip.min.js"></script>
<script src="libs/FileSaver.min.js"></script>

<script src="libs/three/build/three.js"></script>
<script src="libs/three/examples/js/controls/TrackballControls.js"></script>


<script src="js/genomeapp/util.js"></script>
<script src="js/genomeapp/interface.js"></script>
<script src="js/genomeapp/viewer.js"></script>
<script src="js/genomeapp/genomeapp.js"></script>


<script src="js/main.js"></script>


<link href="css/bootstrap-slider.min.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<style>
.slider-selection {
-webkit-box-shadow: none;
box-shadow: none; 
}
</style>
<div class="row">
  <div class="col">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link toolbar-link load-link" href="#" data-toggle="modal" data-target="#upload-dialog">
          <i class="fa fa-folder-open text-success" aria-hidden="true"></i> Change population
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link toolbar-link disabled" href="#" id="download-link">
          <i class="fa fa-cloud-download text-info" aria-hidden="true"></i> Download PDB
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link toolbar-link disabled" href="#" data-toggle="modal" data-target="#log-dialog">
          <i class="fa fa-file-text text-warning" aria-hidden="true"></i> Show logs
        </a>
      </li>
    </ul>
  </div>
</div>

<div class="row">  
  <div class="col-12 col-md-8 text-center">
    <div id="viewer" style="width:100%;"></div>
    <div id="trajectory-controls" style="text-align: center;">
      <button id="traj-prev" class="btn btn-outline-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> Previous Structure</button>
      <span id="traj-frame" style='margin-right: 2em; margin-left: 2em;'></span>
      <button id="traj-next" class="btn btn-outline-secondary">Next Structure <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
    </div>
  </div>
  <div class="col-12 col-md-4" id="control-window">
    <div class="row">
      <div class="col col-sm">
        <form id="jobform" action="#" method="post" enctype="multipart/form-data">
          <div class="row mb-4" id="chain-selector-form">
            <div class="col col-4">    
              Load track (BED format)
            </div>
            <div class="col col-8">
              <input type="file" class="form-control" id="track-upload">
              <button type="button" class="btn btn-outline-primary" id="track-upload-btn">Load </button>
            </div>            
          </div>

          <div class="row mb-4">
            <div class="col col-4">    
              View
            </div>
            <div class="col col-4">
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input type="checkbox" name="viewtype" id="tube-box" class="form-check-input" checked>
                  Tube
                </label>
              </div>
            </div>
            <div class="col col-4">
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input type="checkbox" name="viewtype" id="sphere-box" class="form-check-input">
                  Sphere
                </label>
              </div>
            </div>
          </div>
    
          <div class="row mb-4">
            <div class="col col-4">           
              Select structure
            </div>

            <div class="col col-8">
              <input type="number" class="form-control" id="structure-select-ctrl">

            </div> <!-- col col-8 -->
          </div> <!-- row mb-4 -->

          <div class="row mb-4">
            <div class="col col-4">           
              Select chromosome
            </div>

            <div class="col col-8">
              <select class="form-control" id="chromosome-ctrl">

              </select>
            
            </div> <!-- col col-8 -->
          </div> <!-- row mb-4 -->

          <div id="chromosome-ctrl-div"></div>

          <div id="submission_status">
              
          </div>
        </form>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col">
        <div id="result-info">&nbsp;</div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="upload-dialog" tabindex="-1" role="dialog" aria-labelledby="upload-dialog-label" aria-hidden="true">
  <form id="upload-form">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="upload-dialog-label"><i class="fa fa-folder-open" aria-hidden="true"></i>  Select a hss file</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">  
          <div class="form-group">
                <label for="file-field">Select a file</label>
                <select name="input_file" class="form-control" id="file-field">
<?php
$filenames = glob("/var/www/html/genome/*.hss");
echo $filenames;
foreach ($filenames as $filename) {
  $val = basename($filename);
      echo '<option value="'.$filename.'">'.$val.'</option>\n';
}
?>

                </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-success" id="select-file-btn">Open</button>
        </div>
      </div>
    </div>
  </form>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="log-dialog-label" aria-hidden="true" id="log-dialog">
  <div class="modal-dialog modal-lg">
    
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="log-dialog-label">Logs</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Parameters</h5>
        <pre><div class="alert alert-warning" id="log-prm" role="alert"></div></pre>
        <h5>Command</h5>
        <pre><div class="alert alert-warning" id="log-cmd" role="alert"></div></pre>
        <h5>Output</h5>
        <pre><div class="alert alert-info" id="log-out" role="alert"></div></pre>
        <h5>Standard Error</h5>
        <pre><div class="alert alert-danger" id="log-err" role="alert"></div></pre>
      </div>
    </div>
  </div>
</div>

